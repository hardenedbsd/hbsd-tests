/*-
 * Copyright (c) 2023-2024 by Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#include <sys/param.h>
#include <sys/sysctl.h>
#include <sys/user.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <kvm.h>
#include <sys/queue.h>
#include <sys/socket.h>
#include <libprocstat.h>

#include <sys/mman.h>

#include "libhbsdtest.h"

static struct kinfo_vmentry *find_addr_in_proc(pid_t, void *);

bool
proc_contains_stack_addr(pid_t pid, void *addr)
{
	struct kinfo_vmentry *vme;
	bool res;

	vme = find_addr_in_proc(pid, addr);
	if (vme == NULL) {
		return (false);
	}

	res = (vme->kve_flags & KVME_FLAG_GROWS_DOWN) == KVME_FLAG_GROWS_DOWN;

	free(vme);
	return (res);
}

bool
proc_contains_map_with_prot(pid_t pid, int prot)
{
	return (is_addr_mapped_with_prot(pid, NULL, prot));
}

bool
is_addr_mapped_with_prot(pid_t pid, void *addr, int prot)
{
	struct kinfo_vmentry *vme;
	bool res;

	vme = find_addr_in_proc(pid, addr);
	if (vme == NULL) {
		return (false);
	}

	res = (vme->kve_protection & prot) == prot;

	free(vme);
	return (res);
}

static struct kinfo_vmentry *
find_addr_in_proc(pid_t pid, void *addr)
{
	struct kinfo_vmentry *vme;
	struct kinfo_vmentry *ret;
	unsigned int nproc, nvme;
	struct kinfo_proc *proc;
	struct procstat *ps;
	size_t i;

	ps = procstat_open_sysctl();
	if (ps == NULL) {
		fprintf(stderr, "[-] Could not open procstat\n");
		return (false);
	}

	nproc = 0;
	proc = procstat_getprocs(ps, KERN_PROC_PID, (int)pid, &nproc);
	if (proc == NULL || nproc == 0) {
		fprintf(stderr, "[-] Could not get proc[%d]\n", pid);
		return (false);
	}

	nvme = 0;
	vme = procstat_getvmmap(ps, proc, &nvme);
	if (vme == NULL || nvme == 0) {
		fprintf(stderr, "[-] Could not get vm map\n");
		return (false);
	}

	ret = NULL;
	for (i = 0; i < nvme; i++) {
		if (addr != NULL && (addr < (void *)vme[i].kve_start ||
		    addr > (void *)vme[i].kve_end)) {
			continue;
		}
		ret = calloc(1, sizeof(*ret));
		if (ret == NULL) {
			goto end;
		}
		memmove(ret, &vme[i], sizeof(*ret));
		break;
	}

	if (i == nvme) {
		return (NULL);
	}

end:
	procstat_freevmmap(ps, vme);
	procstat_freeprocs(ps, proc);
	return (ret);
}
