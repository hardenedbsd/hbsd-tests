#ifndef _LIBHBSDTEST_H
#define	_LIBHBSDTEST_H

#include <stdbool.h>

bool proc_contains_map_with_prot(pid_t, int);
bool is_addr_mapped_with_prot(pid_t, void *, int);
bool proc_contains_stack_addr(pid_t, void *);

#endif /* !_LIBHBSDTEST_H */
