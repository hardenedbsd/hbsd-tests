#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <signal.h>

#include <sys/types.h>
#include <sys/mman.h>

#include <libhbsdtest.h>

static void
sighandler(int signo)
{
}

int
main(int argc, char *argv[])
{
	struct sigaction sa;
	stack_t altstack;
	char *stackaddr;

	stackaddr = malloc(SIGSTKSZ);
	if (stackaddr == NULL) {
		perror("malloc");
		return (1);
	}

	memset(&altstack, 0, sizeof(altstack));
	altstack.ss_size = SIGSTKSZ;
	altstack.ss_sp = stackaddr;
	if (sigaltstack(&altstack, NULL)) {
		perror("sigaltstack(malloc)");
	}

	stackaddr = mmap(NULL, altstack.ss_size, PROT_READ | PROT_WRITE,
	    MAP_SHARED | MAP_ANON | MAP_STACK, -1, 0);
	if (stackaddr == (char *)MAP_FAILED) {
		perror("mmap");
		return (1);
	}

#if 0
	if (!proc_contains_stack_addr(getpid(), stackaddr + altstack.ss_size)) {
		printf("Stack allocation %p is not a stack!\n", stackaddr + altstack.ss_size);
		getc(stdin);
		return (1);
	}
#endif

	errno = 0;
	altstack.ss_sp = stackaddr;
	if (sigaltstack(&altstack, NULL)) {
		perror("sigaltstack(mmap)");
		return (1);
	}

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sighandler;
	sa.sa_flags = SA_ONSTACK;
	sigaction(SIGALRM, &sa, NULL);

#if 1
	munmap(stackaddr, altstack.ss_size);
#else
	memset(stackaddr, 0xff, altstack.ss_size);
#endif

	alarm(1);
	sleep(10);

	return (0);
}
