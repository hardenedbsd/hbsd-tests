/*-
 * Copyright (c) 2023 by Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/mman.h>

#include <libhbsdtest.h>

#define MAPLEN	(getpagesize()*10)

static pid_t pid;

int
main(int argc, char *argv[])
{
	int prot;
	void *p;

	if (getuid()) {
		fprintf(stderr, "[-] Please run this tool as root\n");
		exit(1);
	}

	pid = getpid();

	/* Ensure that a normal mapping cannot be created with WX */
	prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	p = mmap(NULL, MAPLEN, prot, MAP_ANON | MAP_SHARED, -1, 0);
	if (p == MAP_FAILED) {
		perror("PAGEEXEC: mmap(RWX)");
		exit(1);
	}

	if (is_addr_mapped_with_prot(pid, p, PROT_WRITE | PROT_EXEC)) {
		fprintf(stderr, "PAGEEXEC: mmap(RWX) call successful\n");
		exit(2);
	}

	mprotect(p, MAPLEN, prot);
	if (is_addr_mapped_with_prot(pid, p, PROT_WRITE | PROT_EXEC)) {
		fprintf(stderr, "PAGEEXEC: mprotect(RWX) call successful\n");
		exit(2);
	}

	munmap(p, MAPLEN);

	/* Ensure that a stack mapping cannot be created with WX */
	p = mmap(NULL, MAPLEN, prot, MAP_STACK | MAP_SHARED, -1, 0);
	if (p == MAP_FAILED) {
		perror("PAGEEXEC: mmap(stack, RWX)");
		exit(1);
	}
	if (is_addr_mapped_with_prot(pid, p, PROT_WRITE | PROT_EXEC)) {
		fprintf(stderr, "PAGEEXEC: mmap(stack, RWX) call successful\n");
		exit(2);
	}

	prot |= PROT_EXEC;
	mprotect(p, MAPLEN, prot);
	if (is_addr_mapped_with_prot(pid, p, PROT_WRITE | PROT_EXEC)) {
		fprintf(stderr, "PAGEEXEC: mprotect(stack, RWX) call successful\n");
		exit(2);
	}

	exit(0);
}
